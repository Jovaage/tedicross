"use strict";

/**************************
 * Import important stuff *
 **************************/

const Application = require('../Application');
const Discord = require('discord.js');
const md2html = require('./md2html');
const MessageMap = require('../MessageMap');

/**********************
 * The setup function *
 **********************/

// eslint-disable-next-line valid-jsdoc
/**
 * Sets up the receiving of Discord messages, and relaying them to Telegram
 *
 * @param {'discord.js'.Client} discordBot    The Discord bot
 * @param {BotAPI} telegramBot    The Telegram bot
 * @param {DiscordUserMap} discordUserMap    A map between discord users and their IDs
 * @param {MessageMap} messageMap    Map between IDs of messages
 */
function discord2telegram(discordBot, telegramBot, discordUserMap, messageMap) {
	// Save the bot's known users when the bot is ready
	discordBot.on('ready', () => {
		// Save the bot's usermap
		Object.keys(discordBot.users).forEach((userId, users) => {
			const { username } = users[userId];

			// Store the UserID/Username mapping
			if (username && userId) {
				discordUserMap.mapIDToUsername(userId, username);
			}
		});
	});

	// Listen for presence to get name/ID mapping
	discordBot.on('presenceUpdate', (oldMember, newMember) => {
		// Get info about the user
		const userName = newMember.user.username;
		const userId = newMember.user.id;

		// Store the UserID/Username mapping
		discordUserMap.mapIDToUsername(userId, userName);
	});

	// Listen for Discord messages
	discordBot.on('message', (message) => {
		// Check if this is a request for chat info
		if (message.cleanContent.toLowerCase() === `@${discordBot.user.username} chatinfo`.toLowerCase()) {
			// It is. Give it
			message.reply(`channelID: ${message.channel.id}${"\n"}serverId: ${message.guild.id}${"\n"}`);

			return;
		}

		// Get info about the sender
		const senderName = message.author.username + (Application.settings.telegram.colonAfterSenderName ? ":" : "");
		const senderId = message.author.id;

		// Store the UserID/Username mapping
		discordUserMap.mapIDToUsername(senderId, senderName);

		// Don't do anything with the bot's own messages
		if (senderId === discordBot.user.id) return;

		// Check if the message is from the correct server
		if (message.channel.guild.id !== Application.settings.discord.serverID) {
			// The message is from the wrong server. Inform the sender that this is a private bot
			message.reply(
				"This is an instance of a TediCross bot, bridging a chat in Telegram with one in Discord. " +
				"If you wish to use TediCross yourself, please download and create an instance. " +
				"You may ask @Suppen on Telegram for help"
			);
			return;
		}

		// Check if the message is from the correct chat
		if (message.channel.id !== Application.settings.discord.channelID) return;

		// Check for attachments and pass them on
		message.attachments.forEach(({ url }) => {
			telegramBot
				.sendMessage({
					chat_id: Application.settings.telegram.chatID,
					// TODO: Fix this non-escaped newline
					text: `<b>${senderName}</b>\n<a href="${url}">${url}</a>`,
					parse_mode: 'HTML',
				})
				// TODO: Check what type of error this actually is
				.catch(err => Application.logger.error("Telegram did not accept an attachment:", err));
		});

		// Check the message for embeds
		message.embeds.forEach((embed) => {
			// Construct the text to send
			let text = `<b>${senderName}</b>\n<a href="${embed.url}">${embed.title}</a>\n`;
			if (embed.description !== undefined) {
				text = `${text}${md2html(embed.description)}${"\n"}`;
			}
			embed.fields.forEach((field) => {
				text = `${text}${"\n"}<b>${field.name}</b>${"\n"}${field.value}${"\n"}`;
			});

			// Send it
			telegramBot
				.sendMessage({
					text,
					chat_id: Application.settings.telegram.chatID,
					parse_mode: 'HTML',
					disable_web_page_preview: true,
				})
				// TODO: Check what type of error this actually is
				.catch(err => Application.logger.error("Telegram did not accept an embed:", err));
		});

		// Check if there is an ordinary text message
		if (message.cleanContent) {
			// Modify the message to fit Telegram
			const processedMessage = md2html(message.cleanContent);

			// Pass the message on to Telegram
			telegramBot
				.sendMessage({
					chat_id: Application.settings.telegram.chatID,
					// TODO: Fix this non-escaped newline
					text: `<b>${senderName}</b>\n${processedMessage}`,
					parse_mode: 'HTML',
				})
				.then((tgMessage) => {
					// Make the mapping so future edits can work
					messageMap.insert(MessageMap.DISCORD_TO_TELEGRAM, message.id, tgMessage.message_id);
				})
				// TODO: Check what type of error this actually is
				.catch((err) => {
					Application.logger.error("Telegram did not accept a message:", err);
					Application.logger.error("Failed message:", err);
				});
		}
	});

	/**
	 * Listen for message edits
	 */
	discordBot.on('messageUpdate', (oldMessage, newMessage) => {
		// Don't do anything with the bot's own messages
		if (newMessage.author.id === discordBot.user.id) return;

		// Get the corresponding Telegram message ID
		Promise.resolve()
			.then(() => messageMap.getCorresponding(MessageMap.DISCORD_TO_TELEGRAM, newMessage.id))
			.then((tgMessageId) => {
				// Get info about the sender
				const senderName = newMessage.author.username +
					(Application.settings.telegram.colonAfterSenderName ? ":" : "");

				// Modify the message to fit Telegram
				const processedMessage = md2html(newMessage.cleanContent);

				// Send the update to Telegram
				return telegramBot.editMessageText({
					chat_id: Application.settings.telegram.chatID,
					message_id: tgMessageId,
					// TODO: Fix this non-escaped newline
					text: `<b>${senderName}</b>\n${processedMessage}`,
					parse_mode: 'HTML',
				});
			})
			// TODO: Check what type of error this actually is
			.catch(err => Application.logger.error("Could not edit Telegram message:", err));
	});

	// Start the Discord bot
	discordBot.login(Application.settings.discord.auth.token)
		.catch(err => Application.logger.error("Could not authenticate the Discord bot:", err));

	// Listen for the 'disconnected' event
	discordBot.on('disconnected', (evt) => {
		Application.logger.error("Discord bot disconnected!", evt);
		telegramBot
			.sendMessage({
				chat_id: Application.settings.telegram.chatID,
				text: "**TEDICROSS**\nThe discord side of the bot disconnected! Please check the log",
			})
			.catch(err => Application.logger.error("Could not send message to Telegram:", err));
	});

	// Listen for debug messages
	if (Application.settings.debug) {
		discordBot.on('debug', (str) => {
			Application.logger.log(str);
		});

		// Check the Discord bot's status every now and then
		setInterval(() => {
			if (discordBot.status !== Discord.Constants.Status.READY) {
				let actualStatus = null;
				switch (discordBot.status) {
					case Discord.Constants.Status.CONNECTING:
						actualStatus = 'CONNECTING';
						break;
					case Discord.Constants.Status.RECONNECTING:
						actualStatus = 'RECONNECTING';
						break;
					case Discord.Constants.Status.IDLE:
						actualStatus = 'IDLE';
						break;
					case Discord.Constants.Status.NEARLY:
						actualStatus = 'NEARLY';
						break;
					case Discord.Constants.Status.DISCONNECTED:
						actualStatus = 'DISCONNECTED';
						break;
					default:
						actualStatus = 'UNKNOWN';
						break;
				}
				Application.logger.error(`Discord status not ready! Status is'${actualStatus}'`);
			}
		}, 1000);
	}

	// Make a promise which resolves when the discord bot is ready
	discordBot.ready = new Promise((resolve) => {
		// Listen for the 'ready' event
		discordBot.on('ready', () => {
			// Log the event
			Application.logger.info(`Discord: ${discordBot.user.username} (${discordBot.user.id})`);

			// Mark the bot as ready
			resolve();
		});
	});
}

/*****************************
 * Export the setup function *
 *****************************/

module.exports = discord2telegram;
