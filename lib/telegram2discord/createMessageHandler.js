"use strict";

/**************************
 * Import important stuff *
 **************************/

const Application = require('../Application');
const curry = require('lodash/curry');

/********************************************
 * Create the createMessageHandler function *
 ********************************************/

/**
 * Curryed function creating handlers handling messages which should not be relayed,
 * and passing through those which should
 *
 * @param {BotAPI} telegramBot  The Telegram bot
 * @param {Function} func       The message handler to wrap
 * @param {Message} message     The Telegram message triggering the wrapped function
 */
const createMessageHandler = curry((telegramBot, func, message) => {
	if (
		message.text !== undefined &&
		telegramBot.me !== undefined &&
		message.text.toLowerCase() === `@${telegramBot.me.username} chatinfo`.toLowerCase()
	) {
		// This is a request for chat info. Give it, no matter which chat this is from
		telegramBot.sendMessage({
			chat_id: message.chat.id,
			text: `chatID: ${message.chat.id}`,
		});
	// Check if the message came from the correct chat
	} else if (message.chat.id !== Application.settings.telegram.chatID) {
		// Tell the sender that this is a private bot
		telegramBot.sendMessage({
			chat_id: message.chat.id,
			text: `This is an instance of a [TediCross](https://github.com/Suppen/TediCross) bot, bridging a chat in Telegram with one in Discord. If you wish to use TediCross yourself, please download and create an instance. You may ask @Suppen for help`,
			parse_mode: 'markdown',
		})
			.catch((err) => {
				// TODO: Check what type of error this actually is
				// Hmm... Could not send the message for some reason TODO Do something about this
				console.error("Could not tell user to get their own TediCross instance:", err, message);
			});
	} else {
		// Do the thing
		func(message);
	}
});

/***********************
 * Export the function *
 ***********************/

module.exports = createMessageHandler;
