"use strict";

/**************************
 * Import important stuff *
 **************************/

const Application = require('../Application');

/**************************************
 * Create the getDisplayName function *
 **************************************/

/**
 * Gets the display name of a user
 *
 * @param {Object} user    A user object
 * @param {Object} chat    A chat object
 *
 * @return {String}    The user's display name
 */
function getDisplayName(user, chat) {
	// Handle for the result
	let displayName = null;

	try {
		// Extract the username
		displayName = user.username;

		// Check whether or not to use names instead (or if the username does not exist)
		if (!displayName || Application.settings.telegram.useFirstNameInsteadOfUsername) {
			displayName = user.first_name;
		}
		// TODO: Check what type of error this actually is
	} catch (err) {
		// No user given. Use the chat title instead
		displayName = chat.title;
	}

	return displayName;
}

/***********************
 * Export the function *
 ***********************/

module.exports = getDisplayName;
