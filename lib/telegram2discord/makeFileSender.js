"use strict";

/**************************
 * Import important stuff *
 **************************/

const Application = require('../Application');

/**************************************
 * Create the makeFileSender function *
 **************************************/

// eslint-disable-next-line valid-jsdoc
/**
 * Creates a function which sends files from Telegram to discord
 *
 * @param {BotAPI} telegramBot            The Telegram bot
 * @param {'discord.js'.Client} discordBot    The Discord bot
 *
 * @returns {Function}    A function which can be used to send files from Telegram to Discord
 */
function makeFileSender(telegramBot, discordBot) {
	/**
	 * Sends a file to Discord
	 *
	 * @param {String} arg.fromName             Display name of the sender
	 * @param {String} arg.fileId               ID of the file to download from Telegram's servers
	 * @param {String} arg.fileName             Name of the file to send
	 * @param {String} [arg.caption]            Additional text to send with the file
	 * @param {Boolean} [arg.resolveExtension]
	 *      Set to true if the bot should try to find the file extension itself,
	 *      in which case it will be appended to the file name. Defaults to false
	 */
	return function fileSender({ fromName, fileId, fileName, caption = "", resolveExtension = false }) {
		// Make the text to send
		const textToSend = `**${fromName}**:\n${caption}`;

		// Handle for the file extension
		let extension = '';

		// Wait for the Discord bot to become ready
		discordBot.ready
			// Start getting the file
			.then(() => telegramBot.getFile({ file_id: fileId }))
			.then((file) => {
				// Get the extension, if necessary
				if (resolveExtension) {
					extension = `.${file.file_path.split(".").pop()}`;
				}
				return telegramBot.helperGetFileStream(file);
			})
			.then((fileStream) => {
				// Create an array of buffers to store the file in
				const buffers = [];

				// Fetch the file
				fileStream.on('data', chunk => buffers.push(chunk));

				// Send the file when it is fetched
				fileStream.on('end', () => {
					discordBot.channels.get(Application.settings.discord.channelID).send(textToSend, {
						file: {
							attachment: Buffer.concat(buffers),
							name: fileName + extension,
						},
					})
					// TODO: Check what type of error this actually is
						.catch(err => Application.logger.error("Discord did not accept a photo:", err));
				});
			});
	};
}

/***********************
 * Export the function *
 ***********************/

module.exports = makeFileSender;
