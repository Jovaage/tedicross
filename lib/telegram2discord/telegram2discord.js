"use strict";

/**************************
 * Import important stuff *
 **************************/

const Application = require('../Application');
const updateGetter = require('./updateGetter');
const handleEntities = require('./handleEntities');
const MessageMap = require('../MessageMap');
const createMessageHandler = require('./createMessageHandler');
const makeFileSender = require('./makeFileSender');
const getDisplayName = require('./getDisplayName');

/**********************
 * The setup function *
 **********************/

// eslint-disable-next-line valid-jsdoc
/**
 * Sets up the receiving of Telegram messages, and relaying them to Discord
 *
 * TODO: Remove EventEmitter from type signature when BotAPI extends it
 * @param {BotAPI|EventEmitter} telegramBot    The Telegram bot
 * @param {"discord.js".Client} discordBot    The Discord bot
 * @param {DiscordUserMap} discordUserMap    A map between discord users and their IDs
 * @param {MessageMap} messageMap    Map between IDs of messages
 */
function telegram2discord(telegramBot, discordBot, discordUserMap, messageMap) {
	// Start longpolling
	updateGetter(telegramBot);

	// Make the file sender
	const sendFile = makeFileSender(telegramBot, discordBot);

	// Create the message handler wrapper
	const messageHandler = createMessageHandler(telegramBot);

	// Set up event listener for text messages from Telegram
	telegramBot.on('text', messageHandler((message) => {
		// Convert the text to Discord format
		const text = handleEntities(discordUserMap, message.text, message.entities);

		// Find out who the message is from
		let fromName = getDisplayName(message.from, message.chat);

		// Check if it is a reply
		if (message.reply_to_message !== undefined) {
			// Get the name of the user this is a reply to
			let inReplyTo = getDisplayName(message.reply_to_message.from, message.chat);

			// Is this a reply to the bot, i.e. to a Discord user?
			if (message.reply_to_message.from.id === telegramBot.me.id) {
				// Get the name of the Discord user this is a reply to
				// TODO Maybe a mapping, so the name doesn't have to be parsed out of the message?
				const dcUsername = message.reply_to_message.text.split("\n")[0];
				inReplyTo = discordUserMap.lookupUsername(dcUsername)
					? `<@${discordUserMap.lookupUsername(dcUsername)}>`
					: dcUsername;
			}

			// Add it to the 'from' text
			fromName = `${fromName} (in reply to ${inReplyTo})`;
		}

		// Check if it is a forwarded message
		if (message.forward_from !== undefined) {
			// Find the name of the user this was forwarded from
			const forwardFrom = getDisplayName(message.forward_from, message.chat);

			// Add it to the 'from' text
			fromName = `${forwardFrom} (forwarded by ${fromName})`;
		}

		// Pass it on to Discord when the dcBot is ready
		discordBot.ready
			.then(() => (
				discordBot.channels.get(Application.settings.discord.channelID).send(`**${fromName}**: ${text}`)
			))
			.then((dcMessage) => {
				// Make the mapping so future edits can work
				messageMap.insert(MessageMap.TELEGRAM_TO_DISCORD, message.message_id, dcMessage.id);
			})
			.catch((err) => {
				// TODO: Check what type of error this actually is
				Application.logger.error("Discord did not accept a text message:", err);
				Application.logger.error("Failed message:", message.text);
			});
	}));

	// Set up event listener for photo messages from Telegram
	telegramBot.on('photo', messageHandler((message) => {
		sendFile({
			fromName: getDisplayName(message.from, message.chat),
			fileId: message.photo[message.photo.length - 1].file_id,
			// Telegram will convert it to jpg no matter what filetype is actually sent
			fileName: 'photo.jpg',
			caption: message.caption,
		});
	}));

	// Set up event listener for stickers from Telegram
	telegramBot.on('sticker', messageHandler((message) => {
		sendFile({
			fromName: getDisplayName(message.from, message.chat),
			fileId: message.sticker.thumb.file_id,
			// Telegram will insist that it is a jpg, but it really is a webp
			fileName: 'sticker.webp',
			caption: message.sticker.emoji,
		});
	}));

	// Set up event listener for filetypes not caught by the other filetype handlers
	telegramBot.on('document', (message) => {
		sendFile({
			fromName: getDisplayName(message.from, message.chat),
			fileId: message.document.file_id,
			fileName: message.document.file_name,
		});
	});

	// Set up event listener for audio messages
	telegramBot.on('audio', (message) => {
		sendFile({
			fromName: getDisplayName(message.from, message.chat),
			fileId: message.audio.file_id,
			fileName: message.audio.title,
			resolveExtension: true,
		});
	});

	// Set up event listener for video messages
	telegramBot.on('video', (message) => {
		sendFile({
			fromName: getDisplayName(message.from, message.chat),
			fileId: message.video.file_id,
			fileName: 'video',
			resolveExtension: true,
		});
	});

	// Set up event listener for message edits
	telegramBot.on('messageEdit', (tgMessage) => {
		// Wait for the Discord bot to become ready
		discordBot.ready
			// Try to get the corresponding message in Discord
			.then(() => messageMap.getCorresponding(MessageMap.TELEGRAM_TO_DISCORD, tgMessage.message_id))
			// Get the message from Discord
			.then(dcMessageId => (
				discordBot.channels.get(Application.settings.discord.channelID).fetchMessage(dcMessageId)
			))
			.then((dcMessage) => {
				// Convert the text to Discord format
				tgMessage.text = handleEntities(discordUserMap, tgMessage.text, tgMessage.entities);

				// Find out who the message is from
				const fromName = getDisplayName(tgMessage.from, tgMessage.chat);

				// Try to edit the message
				return dcMessage.edit(`**${fromName}**: ${tgMessage.text}`);
			})
			// TODO: Check what type of error this actually is
			.catch(err => Application.logger.error("Could not edit Discord message:", err));
	});

	// Make a promise which resolves when the dcBot is ready
	telegramBot.ready = telegramBot.getMe()
		.then((bot) => {
			// Log the bot's info
			Application.logger.info(`Telegram: ${bot.username} (${bot.id})`);

			// Put the data on the bot
			telegramBot.me = bot;
		})
		// TODO: Check what type of error this actually is
		.catch((err) => {
			// Log the error(
			Application.logger.error("Failed at getting the Telegram bot's me-object:", err);

			// Pass it on
			throw err;
		});
}

/*****************************
 * Export the setup function *
 *****************************/

module.exports = telegram2discord;
