"use strict";

/**************************
 * Import important stuff *
 **************************/

// General stuff
const path = require('path');
const Application = require('./lib/Application');
const MessageMap = require('./lib/MessageMap');
const DiscordUserMap = require('./lib/discord2telegram/DiscordUserMap');

// Telegram stuff
const { BotAPI } = require('teleapiwrapper');
const telegram2discord = require('./lib/telegram2discord/telegram2discord');

// Discord stuff
const Discord = require('discord.js');
const discord2telegram = require('./lib/discord2telegram/discord2telegram');

/*************
 * TediCross *
 *************/

// Wrap everything in a try/catch to get a timestamp if a crash occurs
try {
	// Construct the path to the discord user map
	const discordUserMapPath = path.join(__dirname, 'data', 'discord_users.json');

	// Get the discord user map
	const discordUserMap = new DiscordUserMap(discordUserMapPath);

	// Create a message ID map
	const messageMap = new MessageMap();

	// Create a Telegram bot
	const telegramBot = new BotAPI(Application.settings.telegram.auth.token);

	// Create a Discord bot
	const discordBot = new Discord.Client();

	/*********************
	 * Set up the bridge *
	 *********************/

	discord2telegram(discordBot, telegramBot, discordUserMap, messageMap);
	telegram2discord(telegramBot, discordBot, discordUserMap, messageMap);
} catch (err) {
	// Log the timestamp and re-throw the error
	Application.logger.error(err);
	throw err;
}
